#[macro_use]
extern crate lazy_static;
extern crate irc;
extern crate regex;

extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

extern crate rand;

lazy_static! {
    static ref RE: Regex = Regex::new(r"^!spambot (\w+)").unwrap();
}
//const BOI_COUNT: usize = 5;

use irc::client::prelude::*;
use regex::Regex;

use std::env;
use std::fs::File;
use std::path::PathBuf;
use std::io::{BufReader,BufRead};
use std::thread;
use std::time::Duration;
use std::sync::{Arc,Mutex};

use rand::{thread_rng,seq};

mod net;
use net::Net;

fn main(){
    // Load custom config
    let mut config = Config::load("config.toml").expect("Failed to load config");

    let hosts_file = File::open(env::args().skip(1).next().expect("No argument"))
        .expect("Could not open hosts file");

    let hosts_reader = BufReader::new(hosts_file);
    //Create threadpool
    let mut threadpool = Vec::new();

    let mut rng = thread_rng();
    let indices = seq::sample_indices(&mut rng,255,255);


    for (num,line) in hosts_reader.lines().filter_map(|x| x.ok()).enumerate() {
        if let Ok(mut net) = Net::new(&line) {
            //Problem: only 256 possible spambois
            match net.set_nick(indices[num] as u8) {
                Ok(0) => {},
                Ok(_) => threadpool.push(net),
                _ => {}
            }
        }
    }
    config.nickname = Some(format!("spamboi{}",indices[254]));

    let threadpool = Arc::new(Mutex::new(threadpool));




    //let re = Regex::new(r"^!spambot (\w+)").unwrap();
    let mut reactor = IrcReactor::new().unwrap();
    let client = reactor.prepare_client_and_connect(&config).unwrap();
    client.identify().unwrap();


    reactor.register_client_with_handler(client, move |_client, m| {
        //Only handle PRIVMSGs that match the regex
        if let Some(target) = m.response_target() {
            if let Command::PRIVMSG(_,ref message) = m.command {
                if let Some(cap) = RE.captures(message) {
                    handle_message(&cap[1],threadpool.clone(),target.to_string());
                }
            }
        }
        Ok(())
    });

    reactor.run().unwrap();
}
fn handle_message<'a>(
                    //client: &IrcClient, 
                    message_contents: &'a str,
                    threadpool: Arc<Mutex<Vec<Net>>>,
                    target: String
                ) {
    let dir = env::args().skip(2).next().expect("No argument");
    let file_name = message_contents;
    let mut path = PathBuf::new();
    path.push(dir);
    path.push(file_name);
    //Oopen the file and continure if there's no errors,
    //otherwise silently fail (which is okay in irc)
    if let Ok(file) = File::open(path) {
        let buf = BufReader::new(file);
        //Filter out all the io errors and stick the lines in a vec
        //for chunking
        let lines: Vec<String> = buf.lines().filter_map(|x| x.ok()).collect();


        //Lock the hosts pool
        //TODO: error handling
        let mut threadpool = threadpool.lock().unwrap();
        
        //Split message into group of 5 lines,
        //and give the index for index into threadpool list
        for (i,chunk) in lines.chunks(1).enumerate() {
            //TODO: robust thread error handling
            for line in chunk {
                let num = i % threadpool.len();
                //println!("CHAN: {:?} MSG: {:?}",target.clone(),line.to_string());
                match threadpool[num].send(target.clone(),line.to_string()) {
                    Ok(0) => {
                        threadpool.remove(num);
                    },
                    Ok(_) => {},
                    //If a thread fails to respond then cull it
                    Err(_) => {
                        threadpool.remove(num);
                    }
                }
                //thread::sleep(Duration::from_millis(500));
            }
            //Wait so threads don't send out of order
            thread::sleep(Duration::from_millis(900));
        }

    }
}
//fn boi_handler(config: Config, rx: Receiver<(String,String)>) {
//    let mut reactor = IrcReactor::new().unwrap();
//    let client = reactor.prepare_client_and_connect(&config).unwrap();
//    client.identify().unwrap();
//
//
//    reactor.register_client_with_handler(client.clone(), move |_client, _message| {
//        Ok(())
//    });
//
//    thread::spawn(move ||
//        loop {
//            let (chan,msg) = rx.recv().expect("Failed to receive message");
//            match client.send(Command::PRIVMSG(chan,msg)) {
//                Ok(_) => {},
//                Err(e) => println!("Error sending to IRC server: {:?}",e)
//            }
//    });
//
//    reactor.run().unwrap();
//}
