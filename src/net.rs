use serde_json;
use std;
use std::io::{ErrorKind,Write};
use std::net::{TcpStream,ToSocketAddrs};
use std::time::Duration;
#[derive(Serialize)]
struct Message(String,String);
pub struct Net {
   socket: TcpStream, 
}
impl Net {
    pub fn new<'a>(name: &'a str) -> std::io::Result<Net> {
        Ok(Net {
            socket: TcpStream::connect_timeout(
                        &(name,45456u16).to_socket_addrs()?
                            .next().ok_or(ErrorKind::NotFound)?,
                        Duration::new(1,0))?
        })
    }
    pub fn send(&mut self, chan: String, line: String) -> std::io::Result<usize> {
        let vec = match serde_json::to_vec(&Message(chan,line)) {
            Ok(k) => k,
            Err(_) => return Ok(0)
        };
        let num = self.socket.write(&vec)?;
        self.socket.flush()?;
        Ok(num)

    }
    pub fn set_nick(&mut self, num: u8) -> std::io::Result<usize> {
        let buf = [num];
        let num = self.socket.write(&buf)?;
        self.socket.flush()?;
        Ok(num)
    }
}
